import unittest

from sorrisos.app.sorriso import sorriso


class TestSorriso(unittest.TestCase):
    def test_count_smileys(self):
        self.assertEqual(sorriso([':D', ':~)', ';~D', ':)']), 4)
