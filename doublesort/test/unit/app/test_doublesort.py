import unittest

from doublesort.app.doublesort import doublesort


class TestPad652(unittest.TestCase):

    def test_db_sort(self):
        self.assertEqual(doublesort(
            ["Banana", "Orange", "Apple", "Mango", 0, 2, 2]),
            [0, 2, 2, "Apple", "Banana", "Mango", "Orange"]
        )
