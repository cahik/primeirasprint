import unittest

from everydigit.app.everydigit import quadrado

class TestPad651(unittest.TestCase):

    def test_square_digits(self):
        self.assertEqual(quadrado(9119), 811181)
