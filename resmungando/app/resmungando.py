# Desta vez, nenhuma história, nenhuma teoria. Os exemplos abaixo mostram como escrever a função accum:
#
# Exemplos:
#
# accum("abcd") -> "A-Bb-Ccc-Dddd"
# accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
# accum("cwAt") -> "C-Ww-Aaa-Tttt"
# O parâmetro acum é uma string que inclui apenas letras de a..ze A..Z.




###multiplica uma string gradativamente de acordo com o seu index.
def accum(entrada)->str:
    saida = ''
    for index, char in enumerate(entrada, start=1):
        result = char*index
        saida += result.capitalize()

    return saida
