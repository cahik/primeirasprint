
def meiopalavra(palavra):

    meio = len(palavra) // 2

    if len(palavra) % 2 == 0:
        return f"{palavra[meio-1]}{palavra[meio]}"
    else:
        return palavra[meio]


meiopalavra("panelas")

