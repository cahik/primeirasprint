import unittest
from meiodapalavra.app.meiodapalavra import meiopalavra


class TestPad646(unittest.TestCase):

    def test_meiopalavra(self):
        self.assertEqual(meiopalavra("self"), "el")
        self.assertEqual(meiopalavra("selfing"), "f")
        self.assertEqual(meiopalavra("middle"), "dd")
        self.assertEqual(meiopalavra("A"), "A")
        self.assertEqual(meiopalavra("of"), "of")
