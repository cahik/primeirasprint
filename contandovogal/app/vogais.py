def vogal(palavra):

    palavra = palavra.lower()

    vogais = ["a","á","ã","â","é","ê", "e", "i","í","î","ó","ô","õ", "o","ú", "û", "ü", "u"]
    selecionadas = []
    for i in palavra:
        if i in vogais:
            selecionadas.append(i)

    return len(selecionadas)