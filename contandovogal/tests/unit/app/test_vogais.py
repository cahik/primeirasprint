import unittest

from contandovogal.app.vogais import vogal

class TestPad648(unittest.TestCase):
    def test_vogal(self):
        self.assertEqual(vogal("banana"), 3)