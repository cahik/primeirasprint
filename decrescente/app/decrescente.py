# Sua tarefa é criar uma função que possa pegar qualquer número inteiro não negativo como argumento
# e retorná-lo com seus dígitos em ordem decrescente.
# Essencialmente, reorganize os dígitos para criar o número mais alto possível.
#
# Exemplos:
# Entrada: 21445 Saída:54421
#
# Entrada: 145263 Saída:654321

def decre(entrada):
    list = []
    s = entrada
    soma = 0
    for char in entrada:
        soma += int(char)

        list.append(char)
        list.sort(reverse=True)


    return list
