import unittest
from decrescente.app.decrescente import decre



class TestPad650(unittest.TestCase):

    def test_decre(self):
        self.assertEqual(decre("145263"), ['6', '5', '4', '3', '2', '1'])

